package coursera.capstone.symptommanger.client.doctor;

import java.util.ArrayList;

import coursera.capstone.symptommanager.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MedicationListArrayAdapter extends ArrayAdapter<String> {
	
	private ArrayList<String> mMedicationList;

	private final Context mContext;

	public MedicationListArrayAdapter(Context context, ArrayList<String> list) {
		super(context, R.layout.list_item_simple_text);
		this.mContext = context;
		this.mMedicationList = list;
		//sdevikar:
		// put this in a thread
		startThreadAndAddValues();

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// sdevikar:
		// TODO - Replace by recycled view.
		View rowView = inflater.inflate(R.layout.list_item_simple_text, parent,
				false);
		TextView textViewMedicationName = (TextView) rowView
				.findViewById(R.id.textViewListItem);
		textViewMedicationName.setText(mMedicationList.get(position));
		return rowView;
	}

	private void startThreadAndAddValues() {
		this.addAll(mMedicationList);
		
	}
}
