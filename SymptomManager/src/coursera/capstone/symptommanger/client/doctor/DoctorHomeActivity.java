package coursera.capstone.symptommanger.client.doctor;

import coursera.capstone.symptommanager.R;
import coursera.capstone.symptommanager.client.services.Patient;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class DoctorHomeActivity extends ListActivity {

	public static final String INDEX = "INDEX";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(savedInstanceState != null){
			//sdevikar:
			// TODO
			// populate the current list view with the last saved values of patient names
		}
		// use your custom layout
		PatientListArrayAdapter adapter = new PatientListArrayAdapter(this);
		setListAdapter(adapter);
		

	}
	
	  @Override
	  protected void onListItemClick(ListView l, View v, int position, long id) {
	    Intent patientProfileIntent = new Intent(this, PatientProfileActivity.class);
	    patientProfileIntent.putExtra(INDEX, position);
	    startActivity(patientProfileIntent);
	  }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.doctor_home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
