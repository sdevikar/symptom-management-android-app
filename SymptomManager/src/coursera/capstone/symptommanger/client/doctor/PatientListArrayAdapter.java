package coursera.capstone.symptommanger.client.doctor;

import java.util.ArrayList;
import java.util.Date;

import coursera.capstone.symptommanager.R;
import coursera.capstone.symptommanager.client.services.Patient;
import coursera.capstone.symptommanager.client.services.PatientsStub;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class PatientListArrayAdapter extends ArrayAdapter<String> {

	private final Context mContext;
	private ArrayList<Patient> mCurrentPatients;
	private ArrayList<String> mPatientNames;

	private static Patient dummyPatient = new Patient("No Patient Added", -1, Patient.SEX.UNDISCLOSED, null, null);

	public PatientListArrayAdapter(Context context) {
		super(context, R.layout.list_item_patient);
		// Set the values from a local cache, then update from database
		this.mContext = context;
		// while the other background thread is running, put the cached list for
		// display
		readListFromLocalCache();
		// start a thread to update list with patient information
		startThreadAndUpdateValues();
	}

	private void readListFromLocalCache() {
		// Use saved instance state or something here to retreive patients from
		// local cache
	}

	private void startThreadAndUpdateValues() {
		mCurrentPatients = PatientsStub.getInstance().list();
		mPatientNames = getNames(mCurrentPatients);
		this.addAll(mPatientNames);
	}

	private ArrayList<String> getNames(ArrayList<Patient> patients) {
		ArrayList<String> names = new ArrayList<String>();
		for (int i = 0; i < patients.size(); i++) {
			names.add(patients.get(i).getName());
		}
		return names;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// sdevikar:
		// TODO - Replace by recycled view.
		View rowView = inflater.inflate(R.layout.list_item_patient, parent,
				false);
		TextView textViewPatientName = (TextView) rowView
				.findViewById(R.id.textViewPatientName);
		ImageView imageViewPatientPhoto = (ImageView) rowView
				.findViewById(R.id.imagePatientPicture);
		TextView textViewPatientLastSeen = (TextView) rowView
				.findViewById(R.id.textViewPatientLastCheckin);

		textViewPatientName.setText(mPatientNames.get(position));
		Date date = mCurrentPatients.get(position).getLastCheckIn();
		textViewPatientLastSeen.setText(SimpleDateFormat.getDateTimeInstance()
				.format(date));

		// sdevikar:
		// TODO set the imageview here

		return rowView;
	}
}
