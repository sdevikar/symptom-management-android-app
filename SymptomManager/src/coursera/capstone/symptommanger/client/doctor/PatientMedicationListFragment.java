package coursera.capstone.symptommanger.client.doctor;

import java.util.ArrayList;

import coursera.capstone.symptommanager.R;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PatientMedicationListFragment extends Fragment {

	ListView mMedicationListView;
	ArrayAdapter<String> mArrayAdapter;
	private Activity mActivity;
	private static ArrayList<String> mMedicationList;

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static PatientMedicationListFragment newInstance(int sectionNumber, ArrayList<String> medicationList) {

		PatientMedicationListFragment fragment = new PatientMedicationListFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		mMedicationList = medicationList;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_patient_medication_list, container, false);
		mArrayAdapter = new MedicationListArrayAdapter(mActivity, mMedicationList);
		mMedicationListView = (ListView) rootView
				.findViewById(R.id.listViewPatientMedication);
		mMedicationListView.setAdapter(mArrayAdapter);
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = activity;

	}
}
