package coursera.capstone.symptommanger.client.doctor;

import java.util.ArrayList;
import java.util.Locale;

import coursera.capstone.symptommanager.R;
import coursera.capstone.symptommanager.client.services.Patient;
import coursera.capstone.symptommanager.client.services.PatientsStub;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v13.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class PatientProfileActivity extends Activity implements TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v13.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;

	private ActionBar mActionBar;

	// used to store number of tabs
	private int mTabIndex;
	
	private int mSelectedProfileIndex;

	private Patient mPatient;
	private ArrayList<String> mMedicationList;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_profile);

		mTabIndex = 0;

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// enabling action bar app icon and behaving it as toggle button
		mActionBar = getActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			addActionBarTab(i);
		}

		// Extract information from intent that started this activity
		Intent intent = getIntent();
		mSelectedProfileIndex = intent.getIntExtra(DoctorHomeActivity.INDEX, 0);
		mPatient = PatientsStub.getInstance().list().get(mSelectedProfileIndex);
		String name = mPatient.getName();
		setTitle(name);
		mMedicationList = mPatient.getMedicationList();

	}

	public void addActionBarTab(int i) {
		ActionBar.Tab newTab = mActionBar.newTab();
		newTab.setText(mSectionsPagerAdapter.getPageTitle(i));
		newTab.setTabListener(this).setTag(i);
		mActionBar.addTab(newTab);
		mTabIndex++;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			PatientMedicationListFragment x;
			switch(position){
			case 1:
				return PatientMedicationListFragment.newInstance((position +1), mMedicationList);
			case 2:
				return PatientProfileFragment.newInstance(position + 1);
			}
				
			return PatientProfileFragment.newInstance(position + 1);
		}

		@Override
		public int getCount() {
			// Show total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);	

			}
			return null;
		}
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

}
