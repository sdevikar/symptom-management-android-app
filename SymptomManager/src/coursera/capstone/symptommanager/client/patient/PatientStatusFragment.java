package coursera.capstone.symptommanager.client.patient;

import java.util.ArrayList;
import java.util.List;

import coursera.capstone.symptommanager.R;
import coursera.capstone.symptommanager.R.id;
import coursera.capstone.symptommanager.R.layout;
import coursera.capstone.symptommanager.client.services.QuestionsProviderStub;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

//Several Activity and Fragment lifecycle methods are instrumented to emit LogCat output
//so you can follow the class' lifecycle
public class PatientStatusFragment extends Fragment {

	private static final String TAG = "PatientStatusFragment";

	private TextView mQuestionTextView;
	private RadioGroup mAnswersRadioGroup;

	private Button mBackButton;
	private Button mNextButton;

	// stub for getting an array of dummy questions
	private QuestionsProviderStub mQuestionsProvider;

	// current set of questions to be asked
	private Questions mQuestionsYesNo;
	private Questions mQuestionsMultipleOptions;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	// Called to create the content view for this Fragment
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Inflate the layout defined in quote_fragment.xml
		// The last parameter is false because the returned view does not need
		// to be attached to the container ViewGroup
		return inflater.inflate(R.layout.fragment_status_update, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mQuestionsProvider = new QuestionsProviderStub();
		// populate questions to be presented
		mQuestionsYesNo = mQuestionsProvider
				.get(Questions.QUESTION_TYPE_YES_NO);
		mQuestionsMultipleOptions = mQuestionsProvider
				.get(Questions.QUESTION_TYPE_MULTIPLE_CHOICE);

		mQuestionTextView = (TextView) getActivity().findViewById(
				R.id.textViewMultipleChoiceQuestion);
		mAnswersRadioGroup = (RadioGroup) getActivity().findViewById(
				R.id.radioGroupOptions);

		mBackButton = (Button) getActivity().findViewById(
				R.id.buttonBackToPatientHome);
		mNextButton = (Button) getActivity().findViewById(
				R.id.buttonNextToMedsCheck);

		// Setup initial question and options
		setupScreen();

		mNextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setupScreen();
			}

		});

	}

	private void setupScreen() {

		if (mQuestionsMultipleOptions.hasNext()) {
			Question question = mQuestionsMultipleOptions.next();
			String statement = question.getStatement().toString();
			List<String> options = question.getPossibleAnswers();

			mQuestionTextView.setText(statement);
			
			int optionsSize = options.size();
			int numberOfRadioButtons = mAnswersRadioGroup.getChildCount();
			
			if (options.size() != mAnswersRadioGroup.getChildCount()) {
				Log.e(TAG,
						"Possible answers and number of radio buttons available don't match");
				if (options.size() < mAnswersRadioGroup.getChildCount()) {
					// remove the additional views
					for (int i = 0; i < (mAnswersRadioGroup.getChildCount() - options
							.size()); i++)
						mAnswersRadioGroup.removeViewAt(mAnswersRadioGroup
								.getChildCount() - i);
				} else {
					// TODO - Haven't yet figured out how to add an additional
					// radio button in the group dynamically
				}

			} else {
				for (int i = 0; i < mAnswersRadioGroup.getChildCount(); i++) {
					RadioButton radioButton = (RadioButton) mAnswersRadioGroup
							.getChildAt(i);
					radioButton.setText(options.get(i).toString());
				}
			}
		} else {
			// Tell the StatusUpdateActivity to start displaying the yes
			// no type questions fragment
			((StatusUpdateActivity) getActivity())
					.switchToYesNoQuestionsFragment();
		}
		// Intent mMedsCheckIntent = new
		// Intent(getActivity().getBaseContext(),MedsCheckActivity.class);
		// startActivity(mMedsCheckIntent);

	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

}
