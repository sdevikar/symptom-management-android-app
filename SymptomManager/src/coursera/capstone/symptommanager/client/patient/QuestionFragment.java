package coursera.capstone.symptommanager.client.patient;

import java.util.ArrayList;
import java.util.List;

import coursera.capstone.symptommanager.R;
import coursera.capstone.symptommanager.R.id;
import coursera.capstone.symptommanager.R.layout;
import coursera.capstone.symptommanager.client.services.QuestionsProviderStub;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

//Several Activity and Fragment lifecycle methods are instrumented to emit LogCat output
//so you can follow the class' lifecycle
public class QuestionFragment extends Fragment {

	private Button mBackButton;
	private Button mNextButton;
	private TextView mQuestionStatementTextView;

	private QuestionsProviderStub mQuestionsProvider;
	private Questions mQuestions;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	// Called to create the content view for this Fragment
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Inflate the layout defined in quote_fragment.xml
		// The last parameter is false because the returned view does not need
		// to be attached to the container ViewGroup
		return inflater.inflate(R.layout.fragment_meds_check, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// setup the question bank
		mQuestionsProvider = new QuestionsProviderStub();
		mQuestions = mQuestionsProvider.get(Questions.QUESTION_TYPE_YES_NO);

		// initialize UI elements
		mQuestionStatementTextView = (TextView) getActivity().findViewById(
				R.id.textViewMedStatusQuestion);
		mBackButton = (Button) getActivity().findViewById(
				R.id.buttonBackToPainStatus);
		mNextButton = (Button) getActivity().findViewById(
				R.id.button_next_to_next_question);

		/*
		 * Initialize with first question
		 */
		if (mQuestions.hasNext()) {
			Question question = mQuestions.next();
			mQuestionStatementTextView.setText(question.getStatement());
		}

		mNextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mQuestions.hasNext()) {
					Question question = mQuestions.next();
					mQuestionStatementTextView.setText(question.getStatement());
				} else {
					// TODO
					// 0. Save answers
					// 1. Update the home screen to delete the upcoming and
					// move the meds to recent
					// 2. Move the next batch of medicines to upcoming
					getActivity().finish();
				}
			}

		});

	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

}
