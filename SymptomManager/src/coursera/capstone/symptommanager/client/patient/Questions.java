package coursera.capstone.symptommanager.client.patient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

class Question {

	private int type;
	private String statement;
	private List<String> possibleAnswers;
	private String selectedAnswer;

	public Question(String statement, List<String> possibleAnswers,
			String selectedAnswer) {
		this.setStatement(statement);
		this.setPossibleAnswers(possibleAnswers);
		this.setSelectedAnswer(selectedAnswer);
	}

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	public List<String> getPossibleAnswers() {
		return possibleAnswers;
	}

	public void setPossibleAnswers(List<String> possibleAnswers) {
		this.possibleAnswers = possibleAnswers;
	}

	public String getSelectedAnswer() {
		return selectedAnswer;
	}

	public void setSelectedAnswer(String selectedAnswer) {
		this.selectedAnswer = selectedAnswer;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}

public class Questions implements Iterator<Question> {

	public static final int QUESTION_TYPE_YES_NO = 0;
	public static final int QUESTION_TYPE_MULTIPLE_CHOICE = 1;
	private List<Question> list;
	private int index = 0;

	public Questions() {
		list = new ArrayList<Question>();
	}

	public void add(String statement, List<String> possibleAnswers,
			String selectedAnswer) {
		Question q = new Question(statement, possibleAnswers, selectedAnswer);
		list.add(q);
	}

	public String getStatement(int questionIndex) {
		return list.get(questionIndex).getStatement();
	}

	public String getSelectedSelectedAnswer(int questionIndex) {
		return list.get(questionIndex).getSelectedAnswer();
	}

	public List<String> getPossibleAnswers(int questionIndex) {
		return list.get(questionIndex).getPossibleAnswers();
	}

	public void setSelectedSelectedAnswer(int questionIndex,
			String selectedAnswer) {
		list.get(questionIndex).setSelectedAnswer(selectedAnswer);
	}

	public void setPossibleAnswers(int questionIndex,
			List<String> possibleAnswers) {
		list.get(questionIndex).setPossibleAnswers(possibleAnswers);
	}

	@Override
	public boolean hasNext() {
		return (index < list.size());
	}

	@Override
	public Question next() {
		index++;
		return list.get(index - 1);
	}

	@Override
	public void remove() {
	}

}