package coursera.capstone.symptommanager.client.patient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import coursera.capstone.symptommanager.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;

public class PatientHomeActivity extends Activity {
	
	private Button checkinButton;
	
	private ExpandableListAdapter listAdapter;
	private ExpandableListView expListView;
	private List<String> listDataHeader;
	private HashMap<String, List<String>> listDataChild;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_home);
		
		// setup expandable list view
		expListView = (ExpandableListView) findViewById(R.id.expandableListView);
		prepareListData();
		
		// setting list adapter		
		listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        
        // set up checkin button
		setupCheckinButton();
	}
	
	
	/*
	 * Get the list data from calendar
	 * !!!  Consider running this method in the background thread!!!
	 */
	private void prepareListData() {
		//TODO Run in background thread
		//TODO Populate data from Calendar service
		
		listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
 
        // Add header data
        String[] headerArray = getResources().getStringArray(R.array.array_patient_home_items);  
        
        for(String s : headerArray){
        	listDataHeader.add(s);
        }

        List<String> upcomingList = getUpcomingList();		
        List<String> recentList = getRecentList();
        List<String> missedList = getMissedList();
        
        listDataChild.put(listDataHeader.get(0), upcomingList); // Header, Child data
        listDataChild.put(listDataHeader.get(1), recentList);
        listDataChild.put(listDataHeader.get(2), missedList);
	}


	private List<String> getMissedList() {
		// TODO Auto-generated method stub
		return getDummyList();
	}


	private List<String> getRecentList() {
		// TODO Auto-generated method stub
		return getDummyList();
	}


	private List<String> getUpcomingList() {
		// TODO Auto-generated method stub
		return getDummyList();
	}


	private List<String> getDummyList() {
		ArrayList<String> dummyList = new ArrayList<String>();
		dummyList.add("Medicine 1");
		dummyList.add("Medicine 2");
		dummyList.add("Medicine 3");
		return dummyList;
	}


	private void setupCheckinButton()
	{
		checkinButton = (Button) findViewById(R.id.buttonCheckin);
		checkinButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent checkInIntent = new Intent(getBaseContext(), StatusUpdateActivity.class);
				startActivity(checkInIntent);
				
			}});
		
	}
}
