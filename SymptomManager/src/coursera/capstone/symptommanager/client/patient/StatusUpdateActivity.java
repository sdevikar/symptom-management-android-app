package coursera.capstone.symptommanager.client.patient;

import coursera.capstone.symptommanager.R;
import coursera.capstone.symptommanager.R.id;
import coursera.capstone.symptommanager.R.layout;
import coursera.capstone.symptommanager.R.menu;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class StatusUpdateActivity extends Activity {

	private FragmentManager mFragmentManager;

	// Get a reference to the QuotesFragment
	private final PatientStatusFragment mPatientStatusFragment = new PatientStatusFragment();
	private final QuestionFragment mQuestionFragment = new QuestionFragment();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_status_update);

		// Get a reference to the FragmentManager
		mFragmentManager = getFragmentManager();

		// Begin a new FragmentTransaction
		FragmentTransaction fragmentTransaction = mFragmentManager
				.beginTransaction();

		// Add the Patient status fragment to the container
		fragmentTransaction.add(R.id.container, mPatientStatusFragment);

		// Commit the FragmentTransaction
		fragmentTransaction.commit();

		// TODO - Determine what happens here when changes in layout occur
		// Consider changing the layout of activity to host two frames instead
		// of one. So that the second frame can host a second fragment.
		// We might also want to use a different layout xml for activity
		// altogether

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.check_in, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * Called from the medicines fragment when all the patient status questions
	 * are answered
	 */
	public void switchToYesNoQuestionsFragment() {

		// If the PatientStatusFragment has been added, replace it with QuestionFragment
		if (mPatientStatusFragment.isAdded()) {

			// Start a new FragmentTransaction
			FragmentTransaction fragmentTransaction = mFragmentManager
					.beginTransaction();

			// replace it with the QuestionFragment
			fragmentTransaction.replace(R.id.container, mQuestionFragment);

			// Add this FragmentTransaction to the backstack
			fragmentTransaction.addToBackStack(null);

			// Commit the FragmentTransaction
			fragmentTransaction.commit();

			// Force Android to execute the committed FragmentTransaction
			mFragmentManager.executePendingTransactions();

		}
	}
}
