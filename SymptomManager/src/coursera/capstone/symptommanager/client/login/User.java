package coursera.capstone.symptommanager.client.login;



public interface User {
	public static final String ACCESS_LEVEL_PATIENT = "patient";
	public static final String ACCESS_LEVEL_DOCTOR = "doctor";

	public static enum SEX {
		MALE, FEMALE, UNDISCLOSED
	}

	public String getAccessLevel();

	public void setAccessLevel(String accessLevel);

	public long getId();

	public void setId(long id);

	public String getName();

	public void setName(String name);

	public int getAge();

	public void setAge(int age);

	public SEX getSex();

	public void setSex(SEX sex);

	public String getEmail();

	public void setEmail(String email);

}
