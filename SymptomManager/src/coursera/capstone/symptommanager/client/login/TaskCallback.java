/* 
**
** Copyright 2014, Jules White
**
** 
*/
package coursera.capstone.symptommanager.client.login;

public interface TaskCallback<T> {

    public void success(T result);

    public void error(Exception e);

}
