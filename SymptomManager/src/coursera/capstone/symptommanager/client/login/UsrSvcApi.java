package coursera.capstone.symptommanager.client.login;


import java.util.Collection;

import coursera.capstone.symptommanager.client.services.ConcreteUser;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * This interface defines an API for a USERSvc. The
 * interface is used to provide a contract for client/server
 * interactions. The interface is annotated with Retrofit
 * annotations so that clients can automatically convert the
 * 
 * 
 * @author jules
 *
 */
public interface UsrSvcApi {
	
	public static final String PASSWORD_PARAMETER = "password";

	public static final String USERNAME_PARAMETER = "username";

	public static final String NAME_PARAMETER = "name";

	public static final String EMAIL_PARAMETER = "email";
	
	public static final String AGE_PARAMETER = "age";
	
	public static final String TOKEN_PATH = "/oauth/token";
	
	// The path where we expect the UserSvc to live
	public static final String USER_SVC_PATH = "/user";

	// The path to search USERs by title
	public static final String USER_NAME_SEARCH_PATH = USER_SVC_PATH + "/search/findByName";
	
	// The path to search USERs by email
	public static final String USER_EMAIL_SEARCH_PATH = USER_SVC_PATH + "/search/findByEmail";
	
	//The path to search Users by age
	public static final String USER_AGE_SEARCH_PATH = USER_SVC_PATH + "/search/findByAge";
	
	
	@GET(USER_SVC_PATH)
	public Collection<ConcreteUser> getUserList();
	
	@POST(USER_SVC_PATH)
	public Void addUser(@Body ConcreteUser u);
	
	@GET(USER_NAME_SEARCH_PATH)
	public Collection<ConcreteUser> findByName(@Query(NAME_PARAMETER) String title);
	
	@GET(USER_EMAIL_SEARCH_PATH)
	public Collection<ConcreteUser> findByEmail(@Query(EMAIL_PARAMETER) String email);
	
	
}
