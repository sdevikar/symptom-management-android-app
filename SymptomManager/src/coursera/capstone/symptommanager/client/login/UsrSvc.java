/* 
 **
 ** Copyright 2014, Jules White
 **
 ** 
 */
package coursera.capstone.symptommanager.client.login;

import coursera.capstone.symptommanager.client.oauth.SecuredRestBuilder;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;
import android.content.Context;
import android.content.Intent;

public class UsrSvc {

	public static final String CLIENT_ID = "mobile";

	private static UsrSvcApi patientSvc_;

	public static synchronized UsrSvcApi getOrShowLogin(Context ctx) {
		if (patientSvc_ != null) {
			return patientSvc_;
		} else {
			Intent i = new Intent(ctx, LoginActivity.class);
			ctx.startActivity(i);
			return null;
		}
	}

	public static synchronized UsrSvcApi init(String server, String user,
			String pass) {

		patientSvc_ = new SecuredRestBuilder()
				.setLoginEndpoint(server + UsrSvcApi.TOKEN_PATH)
				.setUsername(user)
				.setPassword(pass)
				.setClientId(CLIENT_ID)
				.setClient(
						new ApacheClient(new EasyHttpClient()))
				.setEndpoint(server).setLogLevel(LogLevel.FULL).build()
				.create(UsrSvcApi.class);

		return patientSvc_;
	}
}
