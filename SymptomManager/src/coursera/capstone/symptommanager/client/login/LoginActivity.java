package coursera.capstone.symptommanager.client.login;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;

import coursera.capstone.symptommanager.R;
import coursera.capstone.symptommanager.client.patient.PatientHomeActivity;
import coursera.capstone.symptommanager.client.services.ConcreteUser;
import coursera.capstone.symptommanger.client.doctor.DoctorHomeActivity;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";
	private static String SERVER = "https://10.0.2.2:8443/";

	// Error messages
	private String mNoAccountError;
	private String mDuplicateAccountError;

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		// Load strings
		mNoAccountError = getResources().getString(
				R.string.error_message_no_account);
		mDuplicateAccountError = getResources().getString(
				R.string.error_message_duplicate_account);
		SERVER = getResources().getString(R.string.server_address);

		// Set up the login form.
		mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.loginForm);
		mLoginStatusView = findViewById(R.id.loginStatus);
		mLoginStatusMessageView = (TextView) findViewById(R.id.loginStatusMessage);

		findViewById(R.id.signInButton).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			login(mEmail, mPassword, SERVER);
		}
	}

	/*
	 * Do the actual login using OAuth2.0 flow
	 */
	private void login(String user, String pass, String server) {

		final UsrSvcApi svc = UsrSvc.init(server, user, pass);

		CallableTask.invoke(new Callable<Collection<ConcreteUser>>() {

			@Override
			public Collection<ConcreteUser> call() throws Exception {
				// sdevikar:
				// This service will return the user object corresponding to
				// this email
				// return svc.findByName(mEmail);
				return svc.findByEmail(mEmail);
				// return svc.getUserList();
				// return new ConcreteUser("Doctor1", mEmail, 50, SEX.MALE,
				// User.ACCESS_LEVEL_DOCTOR);

			}
		}, new TaskCallback<Collection<ConcreteUser>>() {

			@Override
			public void success(Collection<ConcreteUser> users) {
				// OAuth 2.0 grant was successful and we
				// can talk to the server
				// See what kind of server is associated with this email and
				// open a screen accordingly

				if (users.size() == 1) {
					User user = users.iterator().next();
					String accessLevel = user.getAccessLevel();

					if (accessLevel.equals(User.ACCESS_LEVEL_DOCTOR)) {
						startActivity(new Intent(LoginActivity.this,
								DoctorHomeActivity.class));
					} else if (accessLevel.equals(User.ACCESS_LEVEL_PATIENT)) {
						startActivity(new Intent(LoginActivity.this,
								PatientHomeActivity.class));
					} else {
						// do nothing 
					}
				} else if (users.size() == 0) {
					// sdevikar:
					// TODO - Maybe register a new account
					Toast.makeText(LoginActivity.this, mNoAccountError,
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(LoginActivity.this, mDuplicateAccountError,
							Toast.LENGTH_LONG).show();
				}

				// After user is authenticated, stop showing the progress bar
				showProgress(false);

			}

			@Override
			public void error(Exception e) {
				Log.e(LoginActivity.class.getName(),
						"Error logging in via OAuth.", e);
				showProgress(false);
				Toast.makeText(
						LoginActivity.this,
						"Login failed, check your Internet connection and credentials.",
						Toast.LENGTH_SHORT).show();
			}
		});

	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
}
