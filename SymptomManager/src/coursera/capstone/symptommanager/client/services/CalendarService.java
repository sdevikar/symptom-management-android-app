// This service will be an add on for syncing with google calendar
// Might use this for managing notifications instead of keeping track of all
// notifications on my own

// e.g. can configure the home screen to display medicine related calendar
// entries upto certain hours from and before current time
// Taking the medicine may remove them from calendar etc.

/*!!!
 * Alert: All the methods in this class are blocking calls and therefore, must be called in a separate thread
 * For now, all methods are run in main thread
 * !!!*/

package coursera.capstone.symptommanager.client.services;

import java.util.ArrayList;
import java.util.TimeZone;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Instances;

public class CalendarService {
	
	private  ContentResolver cr;
	
	public CalendarService(ContentResolver cr){
		this.cr = cr;
	}

	public long makeCalendar(String name, String email) {
		long calId = -1;

		ContentValues values = new ContentValues();
		values.put(Calendars.ACCOUNT_NAME, email);
		values.put(Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
		values.put(Calendars.NAME, name);
		values.put(Calendars.CALENDAR_DISPLAY_NAME, name);
		values.put(Calendars.CALENDAR_COLOR, Color.BLACK);
		values.put(Calendars.CALENDAR_ACCESS_LEVEL, Calendars.CAL_ACCESS_OWNER);
		values.put(Calendars.OWNER_ACCOUNT, email);
		values.put(Calendars.CALENDAR_TIME_ZONE, TimeZone.getAvailableIDs()
				.toString());
		values.put(Calendars.SYNC_EVENTS, 1);

		Uri.Builder builder = CalendarContract.Calendars.CONTENT_URI
				.buildUpon();
		builder.appendQueryParameter(Calendars.ACCOUNT_NAME, email);
		builder.appendQueryParameter(Calendars.ACCOUNT_TYPE,
				CalendarContract.ACCOUNT_TYPE_LOCAL);
		builder.appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER,
				"true");

		Uri uri = cr.insert(builder.build(), values);

		calId = getLocalCalendarId(email);

		return calId;
	}

	/*
	 * This function returns a local calendar ID that was created earlier by
	 * this app
	 */
	private long getLocalCalendarId(String accountName) {

		String[] projection = new String[] { Calendars._ID };

		String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND ("
				+ Calendars.ACCOUNT_TYPE + " = ?))";

		// use the same values as above:

		String[] selArgs = new String[] { accountName,
				CalendarContract.ACCOUNT_TYPE_LOCAL };

		Cursor cursor = cr.query(Calendars.CONTENT_URI, projection, selection,
				selArgs, null);

		if (cursor.moveToFirst()) {

			return cursor.getLong(0);

		} else {
			System.out.println("Cursor did not return any values");
		}

		return -1;

	}

	/*
	 * This function reads the android device for existing calendars in android
	 * and returns its id
	 */
	public long getExistingCalendarId(String email) {
		// Projection array. Creating indices for this array instead of doing
		// dynamic lookups improves performance.
		final String[] EVENT_PROJECTION = new String[] { Calendars._ID, // 0
				Calendars.ACCOUNT_NAME, // 1
				Calendars.CALENDAR_DISPLAY_NAME, // 2
				Calendars.OWNER_ACCOUNT // 3
		};

		// The indices for the projection array above.
		final int PROJECTION_ID_INDEX = 0;
		final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
		final int PROJECTION_DISPLAY_NAME_INDEX = 2;
		final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;
		long calID = 0;

		Cursor cur = null;
		Uri uri = Calendars.CONTENT_URI;
		String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND ("
				+ Calendars.ACCOUNT_TYPE + " = ?) AND ("
				+ Calendars.OWNER_ACCOUNT + " = ?))";
		String[] selectionArgs = new String[] { email, "com.google", email };
		// Submit the query and get a Cursor object back.
		cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);

		// Use the cursor to step through the returned records
		while (cur.moveToNext()) {
			calID = 0;

			String displayName = null;
			String accountName = null;
			String ownerName = null;

			// Get the field values
			calID = cur.getLong(PROJECTION_ID_INDEX);
			displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
			accountName = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX);
			ownerName = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX);

		}
		return calID;
	}

	/*
	 * Function to inserts a recurring calendar event
	 */
	public long insertRecurringEvent(long calID, long startMillis,
			long endMillis) {
		long eventID = 0;

		ContentValues values = new ContentValues();
		values.put(Events.DTSTART, startMillis);
		values.put(Events.DTEND, endMillis);
		values.put(Events.RRULE, "FREQ=DAILY;COUNT=10");
		values.put(Events.TITLE, "Patient Status Check");
		values.put(Events.DESCRIPTION,
				"Check weather patient has taken medicines or not");
		values.put(Events.CALENDAR_ID, calID);
		values.put(Events.EVENT_TIMEZONE, "America/Los_Angeles");
		Uri uri = cr.insert(Events.CONTENT_URI, values);

		eventID = Long.parseLong(uri.getLastPathSegment());
		return eventID;
	}
	
	/*
	 * Returns a list of instances of an event with eventID
	 */
	public ArrayList<String> getRecurringEvents(long eventID,
			long startMillis, long endMillis) {

		ArrayList<String> eventList = new ArrayList<String>();

		final String[] INSTANCE_PROJECTION = new String[] { Instances.EVENT_ID, // 0
				Instances.BEGIN, // 1
				Instances.TITLE // 2
		};

		// The indices for the projection array above.
		final int PROJECTION_ID_INDEX = 0;
		final int PROJECTION_BEGIN_INDEX = 1;
		final int PROJECTION_TITLE_INDEX = 2;

		Cursor cur = null;

		// The ID of the recurring event whose instances you are searching
		// for in the Instances table
		String selection = Instances.EVENT_ID + " = ?";
		String[] selectionArgs = new String[] { "" + eventID };

		// Construct the query with the desired date range.
		Uri.Builder builder = Instances.CONTENT_URI.buildUpon();
		ContentUris.appendId(builder, startMillis);
		ContentUris.appendId(builder, endMillis);

		// Submit the query
		cur = cr.query(builder.build(), INSTANCE_PROJECTION, selection,
				selectionArgs, null);

		while (cur.moveToNext()) {
			String title = null;

			// Get the field values
			title = cur.getString(PROJECTION_TITLE_INDEX);

			eventList.add(title);
		}

		return eventList;
	}

}
