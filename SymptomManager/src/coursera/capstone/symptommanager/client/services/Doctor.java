package coursera.capstone.symptommanager.client.services;

import java.util.ArrayList;

import com.google.common.base.Objects;

import coursera.capstone.symptommanager.client.login.User;

public class Doctor extends ConcreteUser implements User {

	public static final String ID = "ID";

	private ArrayList<String> medicationList;

	public Doctor() {
	}

	public Doctor(String name, int age, SEX sex, String email) {
		
		super(name, email, age,sex, User.ACCESS_LEVEL_DOCTOR);
	}

	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(this.getId(), this.getName(), this.getAge(),
				this.getSex(), this.getEmail());
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Doctor) {
			Doctor other = (Doctor) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(this.getName(), other.getName())
					&& Objects.equal(this.getAge(), other.getAge())
					&& this.getSex() == other.getSex();
		} else {
			return false;
		}
	}

}
