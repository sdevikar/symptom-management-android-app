package coursera.capstone.symptommanager.client.services;

import com.google.common.base.Objects;

import coursera.capstone.symptommanager.client.login.User;

/**
 * 
 * @author sdevikar
 * 
 */
public class ConcreteUser implements User{

	private long id;

	private String name;
	private String email;
	private int age;
	private SEX sex;
	private String accessLevel;

	public ConcreteUser() {
	}

	public ConcreteUser(String name, String email, int age, SEX sex, String accessLevel) {
		super();
		this.name = name;
		this.email = email;
		this.age = age;
		this.setSex(sex);
		this.setAccessLevel(accessLevel);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public SEX getSex() {
		return sex;
	}

	public void setSex(SEX sex) {
		this.sex = sex;
	}
	

	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}
	

	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, email, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(name, email, age/*,sex, dateOfBirth*/);

	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, email, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ConcreteUser) {
			ConcreteUser other = (ConcreteUser) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(name, other.name)
					&& Objects.equal(email, other.email) 
					&& age == other.age
					/*&& sex == other.sex
					&& dateOfBirth.equals(other.dateOfBirth)*/;
			// some other private attribute comparisons
		} else {
			return false;
		}
	}

}
