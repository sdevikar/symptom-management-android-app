package coursera.capstone.symptommanager.client.services;

import java.util.ArrayList;
import java.util.Arrays;

import coursera.capstone.symptommanager.client.patient.Questions;

public class QuestionsProviderStub {

	private static final String[] PAIN_INTENSITY_OPTIONS = new String[] {
			"Well Controlled", "Moderate", "Severe" };
	private static final String[] YES_NO_OPTIONS = new String[] { "Yes", "No" };
	public static final String[] MEDS = new String[] {"Loraxide",
			"Vicodin", "Crocin" };

	private static ArrayList<String>[] patientStatusPossibleAnswers = new ArrayList[2];
	private Questions questions;

	private static final String[] yesNoTypeQuestionStatements = new String[] {
			"Does your pain prevent you from eating/drinking?",
			"Did you take your medicine:" };

	private static final String[] multiplOptionQuestionStatements = new String[] { "How bad is your mouth pain/ sore throat?" };

	public QuestionsProviderStub() {
		patientStatusPossibleAnswers[0] = new ArrayList<String>(
				Arrays.asList(PAIN_INTENSITY_OPTIONS));
		patientStatusPossibleAnswers[1] = new ArrayList<String>(
				Arrays.asList(YES_NO_OPTIONS));
	}

	public Questions get(int questionType) {

		questions = new Questions();
		switch (questionType) {
		case Questions.QUESTION_TYPE_YES_NO:
			// add a bunch of YES NO type questions here
			String statement = yesNoTypeQuestionStatements[0];
			questions.add(statement, Arrays.asList(YES_NO_OPTIONS), null);
			for(String medicine : MEDS){
				statement = yesNoTypeQuestionStatements[1];
				questions.add(statement +"\t" +medicine + "?", Arrays.asList(YES_NO_OPTIONS), null);
			}

			break;
		case Questions.QUESTION_TYPE_MULTIPLE_CHOICE:
			for (String thisStatement : multiplOptionQuestionStatements) {

				questions.add(thisStatement,
						Arrays.asList(PAIN_INTENSITY_OPTIONS), null);
			}

			break;

		}
		return questions;
	}

}
