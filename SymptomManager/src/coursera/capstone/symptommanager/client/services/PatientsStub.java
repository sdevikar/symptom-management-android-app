package coursera.capstone.symptommanager.client.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

import coursera.capstone.symptommanager.client.login.User.SEX;

public class PatientsStub {
	private ArrayList<Patient> patients;
	private Calendar c = Calendar.getInstance();

	private static PatientsStub pStub = null;

	private PatientsStub() {
		patients = new ArrayList<Patient>();
		addDummyPatients();
	}

	public static PatientsStub getInstance() {
		if (pStub == null) {
			pStub = new PatientsStub();
		}
		return pStub;
	}

	private void addDummyPatients() {
		for (int i = 0; i < 10; i++) {

			Date d = new Date(i);
			patients.add(new Patient("Patient" + i, i, SEX.MALE, "patient" + i + "@hospital.com", d));

		}
	}

	public ArrayList<Patient> list() {
		return patients;
	}
}
