package coursera.capstone.symptommanager.client.services;

import java.sql.Date;
import java.util.ArrayList;

import com.google.common.base.Objects;

import coursera.capstone.symptommanager.client.login.User;
import coursera.capstone.symptommanager.client.login.User.SEX;

public class Patient extends ConcreteUser implements User {

	public static final String ID = "ID";

	private Date lastCheckIn;
	private Date dateOfBirth;

	private ArrayList<String> medicationList;

	public Patient() {
	}

	public Patient(String name, int age, SEX sex, String email, Date lastCheckIn) {
		
		super(name, email, age,sex, User.ACCESS_LEVEL_PATIENT);
		this.setLastCheckIn(lastCheckIn);
		medicationList = new ArrayList<String>();
		// add some dummy medicines for now
		// addMedications(QuestionsProviderStub.MEDS);
	}

	public ArrayList<String> getMedicationList() {
		return medicationList;
	}

	public void setMedicationList(ArrayList<String> medicationList) {
		this.medicationList = medicationList;
	}

	public void addMedication(String medName) {
		this.medicationList.add(medName);
	}

	public void addMedications(String[] medNames) {
		for (String thisMed : medNames) {
			this.medicationList.add(thisMed);
		}
	}

	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(this.getId(), this.getName(), this.getAge(),
				this.getSex(), this.getEmail());
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Patient) {
			Patient other = (Patient) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(this.getName(), other.getName())
					&& Objects.equal(this.getAge(), other.getAge())
					&& this.getSex() == other.getSex()
					&& dateOfBirth.equals(other.dateOfBirth);
		} else {
			return false;
		}
	}

	public Date getLastCheckIn() {
		return lastCheckIn;
	}

	public void setLastCheckIn(Date lastCheckIn) {
		this.lastCheckIn = lastCheckIn;
	}

}
