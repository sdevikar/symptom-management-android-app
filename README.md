# README #

This repository is an eclipse workspace for SymptomManager android app

### What is this repository for? ###

* Symptom Management android app
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Pull this repository in your local workspace and import the project as an android project in the workspace
* Compatibility: Android version 4.0 and above
* Server side dependencies: Spring framework
* Database configuration: Spring default. Maybe JPA
* <How to run tests>
* <Deployment instructions>

### Contribution guidelines ###

* <Writing tests>
* <Code review>
* <Other guidelines>

### Who do I talk to? ###

* Author: Swapnil Devikar. 
* Email: swap1712@gmail.com